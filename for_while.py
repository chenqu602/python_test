#-*- coding=utf-8 -*-
__author__ = 'admin'

names = ['Michael', 'Bob', 'Tracy']
for name in names:
    print name


sums = 0
for x in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]:
    sums += x
print sums

sums2 = 0
for x in range(10, 100):
    sums2 += x
print sums2


sum3 = 0
n = 99
while n > 0:
    sum3 += n
    n -= 2
print sum3

birth = input('birth: ')
if birth < 2000:
    print '00前'
else:
    print '00后'