# -*- coding=utf-8 -*-

__author__ = 'admin'

# 默认参数
def power(x, n=2):
    s = 1
    while n > 0:
        n -= 1
        s = s * x
    return s


print power(5, 2)
print power(5)


def add_end(l=[]):
    l.append('END')
    return l

print add_end([1, 2, 3])
print add_end(['x', 'y', 'z'])

# !!!!默认参数必须指向不变对象！add_end(l=[])的定义方式是不正确的
print add_end()
print add_end()


def add_end(l=None):
    if l is None:
        l = []
    l.append('END')
    return l

print add_end()
print add_end()


# 非可变参数
def calc(numbers):
    sums = 0
    for n in numbers:
        sums = sums + n * n
    return sums

print calc([1, 2, 3])
print calc((1, 3, 5, 7))

# 可变参数
# 可变参数允许你传入0个或任意个参数，这些可变参数在函数调用时自动组装为一个tuple
def calc(*numbers):
    sums = 0
    for n in numbers:
        sums = sums + n * n
    return sums

print calc(1, 3, 5, 7)
nums = [1, 2, 3]
print calc(*nums)


# 关键字参数
# 关键字参数允许你传入0个或任意个含参数名的参数，这些关键字参数在函数内部自动组装为一个dict
def person(name, age, **kw):
    print 'name:', name, 'age:', age, 'other:', kw

print person('Michael', 30)
print person('Bob', 35, city='Beijing')
print person('Adam', 45, gender='M', job='Engineer')

kw = {'city': 'Beijing', 'job': 'Engineer'}
print person('Jack', 24, **kw)


# 参数组合
# 参数定义的顺序必须是：必选参数、默认参数、可变参数和关键字参数。
def func(a, b, c=0, *args, **kw):
    print 'a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw

print func(1, 2)
print func(1, 2, c=3)
print func(1, 2, c=3)
print func(1, 2, 3, 'a', 'b')
print func(1, 2, 3, 'c', 'd', m=1, n=2, x=99)


# *args是可变参数，args接收的是一个tuple；
# **kw是关键字参数，kw接收的是一个dict。
args = (1, 2, 3, 4)
kw = {'x': 99}
print func(*args, **kw)