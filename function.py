# -*- coding=utf-8 -*-

__author__ = 'admin'
import math


def move(x, y, step, angle=0):
    nx = x + step * math.cos(angle)
    ny = y - step * math.sin(angle)
    return nx, ny


def my_abs(x):
    if not isinstance(x, (int, float)):
        raise TypeError('bad operand type')
    if x >= 0:
        return x
    else:
        return -x

# 原来返回值是一个tuple！
# 但是，在语法上，返回一个tuple可以省略括号，而多个变量可以同时接收一个tuple，按位置赋给对应的值，
# 所以，Python的函数返回多值其实就是返回一个tuple，但写起来更方便。
x, y = move(100, 100, 60, math.pi/6)
print x,y

r = move(100, 100, 60, math.pi/6)
print r

print my_abs(-5)
print my_abs('x')