#!/usr/bin/python
#-*- coding:utf-8 -*-
__author__ = 'admin'

classmates = ['Michael', 'Bob', 'Tracy']

print len(classmates)
print classmates[0]
print classmates[-1]

classmates.append('Adam')
print classmates

classmates.insert(1, 'Jack')
print classmates

print classmates.pop()
print classmates

print classmates.pop(1)
print classmates

classmates.sort()
print "sort:", classmates

classmates[1] = 'Sarah'
print classmates

#tuple 不可变数组
classmates = ('Michael', 'Bob', 'Tracy')

t = ('a', 'b', ['A', 'B'])
t[2][0] = 'X'
t[2][1] = 'Y'
print t