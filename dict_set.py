__author__ = 'admin'

names = ['Michael', 'Bob', 'Tracy']
scores = [95, 75, 85]

#dict
d = {'Michael': 95, 'Bob': 75, 'Tracy': 85}
print d['Michael']

print d.get('Thomas')
print d.get('Thomas', -1)
print d.get('Bob', -1)


# delete key
print d.pop('Michael')
print d


# set
s = set([1, 1, 2, 2, 3, 3])
print s

s.add(4)
s.add(1)
print s

