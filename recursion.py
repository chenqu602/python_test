# -*- coding=utf-8 -*-
__author__ = 'admin'


def fact(n):
    if n == 1:
        return 1
    return n * fact(n - 1)


print fact(5)

# 注意防止栈溢出
# print fact(1000)


# 尾递归是指，在函数返回的时候，调用自身本身，并且，return语句不能包含表达式。
# 这样，编译器或者解释器就可以把尾递归做优化，使递归本身无论调用多少次，都只占用一个栈帧，不会出现栈溢出的情况。
def fact(n):
    return fact_iter(1, 1, n)


def fact_iter(product, count, max):
    if count > max:
        return product
    return fact_iter(product * count, count + 1, max)
    # 可以看到，return fact_iter(product * count, count + 1, max)仅返回递归函数本身，
    # product * count和count + 1在函数调用前就会被计算，不影响函数调用。

# Python标准的解释器没有针对尾递归做优化，任何递归函数都存在栈溢出的问题。
# 所以，即使把上面的fact(n)函数改成尾递归方式，也会导致栈溢出。
# 有一个针对尾递归优化的decorator，可以参考源码：
# http://code.activestate.com/recipes/474088-tail-call-optimization-decorator/
# 只需要使用这个@tail_call_optimized，就可以顺利计算出fact(1000)

print fact(1000)