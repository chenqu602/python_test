#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'admin'

print ord('A')
print chr(65)
print unichr(19345)

print u'中文'
print u'中'

print u'中文'.encode('utf-8') #\xe4\xb8\xad\xe6\x96\x87
print '\xe4\xb8\xad\xe6\x96\x87'.decode('utf-8')

print 'Hi %s, you have $%d.' % ('Peter', 1000000000)
print '%2d-%02d' % (3, 1)
print '%.2f' % 3.1415926
print '%s' % 3.1415926